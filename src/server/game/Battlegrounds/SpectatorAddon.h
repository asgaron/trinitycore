/*
* Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRINITY_BATTLEGROUND_SPECTATOR
#define TRINITY_BATTLEGROUND_SPECTATOR

#include "ScriptMgr.h"

#define SPECTATOR_ADDON_SPELL_INTERUPTED 99999 
#define SPECTATOR_ADDON_SPELL_CANCELED 99998

enum SpectatorPrefix {
	SPECTATOR_PREFIX_PLAYER,
	SPECTATOR_PREFIX_STATUS,
	SPECTATOR_PREFIX_MAXHP,
	SPECTATOR_PREFIX_CURHP,
	SPECTATOR_PREFIX_MAXPOWER,
	SPECTATOR_PREFIX_CURPOWER,
	SPECTATOR_PREFIX_POWERTYPE,
	SPECTATOR_PREFIX_TARGET,
	SPECTATOR_PREFIX_CLASS,
	SPECTATOR_PREFIX_TEAM,
	SPECTATOR_PREFIX_SPELL,
	SPECTATOR_PREFIX_AURA,
	SPECTATOR_PREFIX_COUNT      // must be at the end of list
};

class SpectatorAddonMsg {
public:
	SpectatorAddonMsg()
	{
		for (uint8 i = 0; i < SPECTATOR_PREFIX_COUNT; ++i)
			prefixFlags[i] = false;

		player = "";
		target = "";
		isAlive = false;
		pClass = CLASS_WARRIOR;
		maxHP = 0;
		maxPower = 0;
		currHP = 0;
		currPower = 0;
		powerType = POWER_MANA;
		spellId = 0;
		castTime = 0;
		team = ALLIANCE;
	}

	void SetPlayer(std::string _player)		{ player = _player;		EnableFlag(SPECTATOR_PREFIX_PLAYER); }
	void SetStatus(bool _isAlive)			{ isAlive = _isAlive;	EnableFlag(SPECTATOR_PREFIX_STATUS); }
	void SetClass(uint8 _class)				{ pClass = _class;		EnableFlag(SPECTATOR_PREFIX_CLASS); }
	void SetTarget(std::string _target)		{ target = _target;		EnableFlag(SPECTATOR_PREFIX_TARGET); }
	void SetTeam(uint32 _team)				{ team = _team;			EnableFlag(SPECTATOR_PREFIX_TEAM); }

	void SetMaxHP(uint16 hp)				{ maxHP = hp;			EnableFlag(SPECTATOR_PREFIX_MAXHP); }
	void SetCurrentHP(uint16 hp)			{ currHP = hp;			EnableFlag(SPECTATOR_PREFIX_CURHP); }
	void SetMaxPower(uint16 power)			{ maxPower = power;		EnableFlag(SPECTATOR_PREFIX_MAXPOWER); }
	void SetCurrentPower(uint16 power)		{ currPower = power;	EnableFlag(SPECTATOR_PREFIX_CURPOWER); }
	void SetPowerType(Powers power)			{ powerType = power;	EnableFlag(SPECTATOR_PREFIX_POWERTYPE); }

	void CastSpell(uint32 _spellId, uint32 _castTime) { spellId = _spellId; castTime = _castTime; EnableFlag(SPECTATOR_PREFIX_SPELL); }
	void CreateAura(uint32 _caster, uint32 _spellId, bool _isDebuff, uint8 _type, int32 _duration, int32 _expire, uint16 _stack, bool _isRemove)
	{
		if (!CanSandAura(_spellId))
			return;

		aCaster = _caster;
		aSpellId = _spellId;
		aIsDebuff = _isDebuff;
		aType = _type;
		aDuration = _duration;
		aExpire = _expire;
		aStack = _stack;
		aRemove = _isRemove;
		EnableFlag(SPECTATOR_PREFIX_AURA);
	}

	static bool SendPacket(SpectatorAddonMsg msg, ObjectGuid receiver)
	{
		std::string addonData = msg.GetMsgData();
		if (addonData == "")
			return false;

		Player* rPlayer = ObjectAccessor::FindPlayer(receiver);
		if (!rPlayer)
			return false;

		WorldPacket data(SMSG_MESSAGECHAT, 200);
		data << uint8(CHAT_MSG_WHISPER);
		data << uint32(LANG_ADDON);
		data << uint64(0);
		data << uint32(LANG_ADDON);                               //language 2.1.0 ?
		data << uint64(0);
		data << uint32(addonData.length() + 1);
		data << addonData;
		data << uint8(CHAT_TAG_NONE);
		rPlayer->GetSession()->SendPacket(&data);

		return true;
	}

	bool SendPacket(ObjectGuid receiver)
	{
		std::string addonData = GetMsgData();
		if (addonData == "")
			return false;

		Player* rPlayer = ObjectAccessor::FindPlayer(receiver);
		if (!rPlayer)
			return false;

		WorldPacket data(SMSG_MESSAGECHAT, 200);
		data << uint8(CHAT_MSG_WHISPER);
		data << uint32(LANG_ADDON);
		data << uint64(0);
		data << uint32(LANG_ADDON);                               //language 2.1.0 ?
		data << uint64(0);
		data << uint32(addonData.length() + 1);
		data << addonData;
		data << uint8(CHAT_TAG_NONE);
		rPlayer->GetSession()->SendPacket(&data);

		return true;
	}

	std::string GetMsgData()
	{
		std::string addonData = "";

		if (!isFilledIn(SPECTATOR_PREFIX_PLAYER))
		{
			TC_LOG_INFO("battleground", "SPECTATOR ADDON: player is not filled in.");
			return addonData;
		}

		std::string msg = "";
		for (uint8 i = 0; i < SPECTATOR_PREFIX_COUNT; ++i)
			if (isFilledIn(i))
			{
				switch (i)
				{
				case SPECTATOR_PREFIX_PLAYER:
					msg += player + ";";
					break;
				case SPECTATOR_PREFIX_TARGET:
					msg += "TRG=" + target + ";";
					break;
				case SPECTATOR_PREFIX_TEAM:
				{
					char buffer[20];
					sprintf(buffer, "TEM=%i;", (uint16)team);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_STATUS:
				{
					char buffer[20];
					sprintf(buffer, "STA=%d;", isAlive);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_CLASS:
				{
					char buffer[20];
					sprintf(buffer, "CLA=%i;", (int)pClass);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_MAXHP:
				{
					char buffer[30];
					sprintf(buffer, "MHP=%i;", maxHP);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_CURHP:
				{
					char buffer[30];
					sprintf(buffer, "CHP=%i;", currHP);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_MAXPOWER:
				{
					char buffer[30];
					sprintf(buffer, "MPW=%i;", maxPower);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_CURPOWER:
				{
					char buffer[30];
					sprintf(buffer, "CPW=%i;", currPower);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_POWERTYPE:
				{
					char buffer[20];
					sprintf(buffer, "PWT=%i;", (uint8)powerType);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_SPELL:
				{
					char buffer[80];
					sprintf(buffer, "SPE=%i,%i;", spellId, castTime);
					msg += buffer;
					break;
				}
				case SPECTATOR_PREFIX_AURA:
				{
					char buffer[300];
					sprintf(buffer, "AUR=%i,%i,%i,%i,%i,%i,%i,0x%X;", aRemove, aStack,
						aExpire, aDuration,
						aSpellId, aType,
						aIsDebuff, aCaster);
					msg += buffer;
					break;
				}
				}
			}

		if (msg != "")
			addonData = "ARENASPEC	" + msg;

		return addonData;
	}

	bool isFilledIn(uint8 prefix) { return prefixFlags[prefix]; }

	static bool CanSandAura(uint32 auraID)
	{
		const SpellInfo *spell = sSpellMgr->GetSpellInfo(auraID);
		if (!spell)
			return false;

		if (spell->SpellIconID == 1)
			return false;

		return true;
	}
private:

	void EnableFlag(uint8 prefix) { prefixFlags[prefix] = true; }
	std::string player;
	bool isAlive;
	std::string target;
	uint8 pClass;

	uint16 maxHP;
	uint16 maxPower;
	uint16 currHP;
	uint16 currPower;
	Powers powerType;

	uint32 spellId;
	uint32 castTime;

	uint32 team;

	// aura data
	uint32 aCaster;
	uint32 aSpellId;
	bool aIsDebuff;
	uint8 aType;
	int32 aDuration;
	int32 aExpire;
	uint16 aStack;
	bool aRemove;

	bool prefixFlags[SPECTATOR_PREFIX_COUNT];
};

#endif