/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "Corpse.h"
#include "Player.h"
#include "MapManager.h"
#include "Transport.h"
#include "Battleground.h"
#include "WaypointMovementGenerator.h"
#include "InstanceSaveMgr.h"
#include "ObjectMgr.h"
#include "Vehicle.h"
#include "World.h"
#include "WardenWin.h"
#include "Appender.h"
#include "Chat.h"

// Movement anticheat defines
//#define ANTICHEAT_DEBUG
#define ANTICHEAT_EXCEPTION_INFO
// End Movement anticheat defines

#define MOVEMENT_PACKET_TIME_DELAY 0

void WorldSession::HandleMoveWorldportAckOpcode(WorldPacket & /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: got MSG_MOVE_WORLDPORT_ACK.");
    HandleMoveWorldportAckOpcode();
}

void WorldSession::HandleMoveWorldportAckOpcode()
{
    // ignore unexpected far teleports
    if (!GetPlayer()->IsBeingTeleportedFar())
        return;

    GetPlayer()->SetSemaphoreTeleportFar(false);

    // get the teleport destination
    WorldLocation const& loc = GetPlayer()->GetTeleportDest();

    // possible errors in the coordinate validity check
    if (!MapManager::IsValidMapCoord(loc))
    {
        LogoutPlayer(false);
        return;
    }

    // get the destination map entry, not the current one, this will fix homebind and reset greeting
    MapEntry const* mEntry = sMapStore.LookupEntry(loc.GetMapId());
    InstanceTemplate const* mInstance = sObjectMgr->GetInstanceTemplate(loc.GetMapId());

    // reset instance validity, except if going to an instance inside an instance
    if (GetPlayer()->m_InstanceValid == false && !mInstance)
        GetPlayer()->m_InstanceValid = true;

    Map* oldMap = GetPlayer()->GetMap();
    Map* newMap = sMapMgr->CreateMap(loc.GetMapId(), GetPlayer());

    if (GetPlayer()->IsInWorld())
    {
        TC_LOG_ERROR("network", "%s %s is still in world when teleported from map %s (%u) to new map %s (%u)", GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), oldMap->GetMapName(), oldMap->GetId(), newMap ? newMap->GetMapName() : "Unknown", loc.GetMapId());
        oldMap->RemovePlayerFromMap(GetPlayer(), false);
    }

    // relocate the player to the teleport destination
    // the CanEnter checks are done in TeleporTo but conditions may change
    // while the player is in transit, for example the map may get full
    if (!newMap || !newMap->CanEnter(GetPlayer()))
    {
        TC_LOG_ERROR("network", "Map %d (%s) could not be created for player %d (%s), porting player to homebind", loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown", GetPlayer()->GetGUIDLow(), GetPlayer()->GetName().c_str());
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    float z = loc.GetPositionZ();
    if (GetPlayer()->HasUnitMovementFlag(MOVEMENTFLAG_HOVER))
        z += GetPlayer()->GetFloatValue(UNIT_FIELD_HOVERHEIGHT);
    GetPlayer()->Relocate(loc.GetPositionX(), loc.GetPositionY(), z, loc.GetOrientation());

    GetPlayer()->ResetMap();
    GetPlayer()->SetMap(newMap);

    GetPlayer()->SendInitialPacketsBeforeAddToMap();
    if (!GetPlayer()->GetMap()->AddPlayerToMap(GetPlayer()))
    {
        TC_LOG_ERROR("network", "WORLD: failed to teleport player %s (%d) to map %d (%s) because of unknown reason!",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUIDLow(), loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown");
        GetPlayer()->ResetMap();
        GetPlayer()->SetMap(oldMap);
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    // battleground state prepare (in case join to BG), at relogin/tele player not invited
    // only add to bg group and object, if the player was invited (else he entered through command)
    if (_player->InBattleground())
    {
        // cleanup setting if outdated
        if (!mEntry->IsBattlegroundOrArena())
        {
            // We're not in BG
            _player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
            // reset destination bg team
            _player->SetBGTeam(0);
        }
        // join to bg case
        else if (Battleground* bg = _player->GetBattleground())
        {
            if (_player->IsInvitedForBattlegroundInstance(_player->GetBattlegroundId()))
                bg->AddPlayer(_player);
        }
    }

    GetPlayer()->SendInitialPacketsAfterAddToMap();

    // flight fast teleport case
    if (GetPlayer()->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
    {
        if (!_player->InBattleground())
        {
            // short preparations to continue flight
            FlightPathMovementGenerator* flight = (FlightPathMovementGenerator*)(GetPlayer()->GetMotionMaster()->top());
            flight->Initialize(GetPlayer());
            return;
        }

        // battleground state prepare, stop flight
        GetPlayer()->GetMotionMaster()->MovementExpired();
        GetPlayer()->CleanupAfterTaxiFlight();
    }

    // resurrect character at enter into instance where his corpse exist after add to map
    Corpse* corpse = GetPlayer()->GetCorpse();
    if (corpse && corpse->GetType() != CORPSE_BONES && corpse->GetMapId() == GetPlayer()->GetMapId())
    {
        if (mEntry->IsDungeon())
        {
            GetPlayer()->ResurrectPlayer(0.5f, false);
            GetPlayer()->SpawnCorpseBones();
        }
    }

    bool allowMount = !mEntry->IsDungeon() || mEntry->IsBattlegroundOrArena();
    if (mInstance)
    {
        Difficulty diff = GetPlayer()->GetDifficulty(mEntry->IsRaid());
        if (MapDifficulty const* mapDiff = GetMapDifficultyData(mEntry->MapID, diff))
        {
            if (mapDiff->resetTime)
            {
                if (time_t timeReset = sInstanceSaveMgr->GetResetTimeFor(mEntry->MapID, diff))
                {
                    uint32 timeleft = uint32(timeReset - time(NULL));
                    GetPlayer()->SendInstanceResetWarning(mEntry->MapID, diff, timeleft, true);
                }
            }
        }
        allowMount = mInstance->AllowMount;
    }

    // mount allow check
    if (!allowMount)
        _player->RemoveAurasByType(SPELL_AURA_MOUNTED);

    // update zone immediately, otherwise leave channel will cause crash in mtmap
    uint32 newzone, newarea;
    GetPlayer()->GetZoneAndAreaId(newzone, newarea);
    GetPlayer()->UpdateZone(newzone, newarea);

    // honorless target
    if (GetPlayer()->pvpInfo.IsHostile)
        GetPlayer()->CastSpell(GetPlayer(), 2479, true);

    // in friendly area
    else if (GetPlayer()->IsPvP() && !GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
        GetPlayer()->UpdatePvP(false, false);

    // resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    //lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMoveTeleportAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "MSG_MOVE_TELEPORT_ACK");
    ObjectGuid guid;

    recvData >> guid.ReadAsPacked();

    uint32 flags, time;
    recvData >> flags >> time;

	Player* plrMover = _player->m_mover->ToPlayer();

	if (!plrMover || !plrMover->IsBeingTeleportedNear())
        return;

	if (guid != plrMover->GetGUID())
        return;

	plrMover->SetSemaphoreTeleportNear(false);

	uint32 old_zone = plrMover->GetZoneId();

	WorldLocation const& dest = plrMover->GetTeleportDest();

	plrMover->UpdatePosition(dest, true);

    uint32 newzone, newarea;
	plrMover->GetZoneAndAreaId(newzone, newarea);
	plrMover->UpdateZone(newzone, newarea);

    // new zone
    if (old_zone != newzone)
    {
        // honorless target
		if (plrMover->pvpInfo.IsHostile)
			plrMover->CastSpell(plrMover, 2479, true);

        // in friendly area
		else if (plrMover->IsPvP() && !plrMover->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
			plrMover->UpdatePvP(false, false);
    }

    // resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    //lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMovementOpcodes(WorldPacket& recvData)
{
    uint16 opcode = recvData.GetOpcode();

    Unit* mover = _player->m_mover;

    ASSERT(mover != NULL);                      // there must always be a mover

    Player* plrMover = mover->ToPlayer();

    // ignore, waiting processing in WorldSession::HandleMoveWorldportAckOpcode and WorldSession::HandleMoveTeleportAck
    if (plrMover && plrMover->IsBeingTeleported())
    {
        recvData.rfinish();                     // prevent warnings spam
        return;
    }

    /* extract packet */
    ObjectGuid guid;

    recvData >> guid.ReadAsPacked();

    MovementInfo movementInfo;
    movementInfo.guid = guid;
    ReadMovementInfo(recvData, &movementInfo);

    recvData.rfinish();                         // prevent warnings spam

    // pussywizard: typical check for incomming movement packets
    if (!mover || !mover->IsInWorld() || mover->IsDuringRemoveFromWorld() || guid != mover->GetGUID())
        return;

    if (!movementInfo.pos.IsPositionValid())
    {
        recvData.rfinish();                     // prevent warnings spam
        return;
    }

    if (movementInfo.flags & MOVEMENTFLAG_ONTRANSPORT)
    {
		// T_POS ON VEHICLES!
		if (mover->GetVehicle())
			movementInfo.transport.pos = mover->m_movementInfo.transport.pos;

        // transports size limited
        // (also received at zeppelin leave by some reason with t_* as absolute in continent coordinates, can be safely skipped)
        if (movementInfo.transport.pos.GetPositionX() > 75.0f || movementInfo.transport.pos.GetPositionY() > 75.0f || movementInfo.transport.pos.GetPositionZ() > 75.0f ||
            movementInfo.transport.pos.GetPositionX() < -75.0f || movementInfo.transport.pos.GetPositionY() < -75.0f || movementInfo.transport.pos.GetPositionZ() < -75.0f)
        {
            recvData.rfinish();                   // prevent warnings spam
            return;
        }

        if (!Trinity::IsValidMapCoord(movementInfo.pos.GetPositionX() + movementInfo.transport.pos.GetPositionX(), movementInfo.pos.GetPositionY() + movementInfo.transport.pos.GetPositionY(),
            movementInfo.pos.GetPositionZ() + movementInfo.transport.pos.GetPositionZ(), movementInfo.pos.GetOrientation() + movementInfo.transport.pos.GetOrientation()))
        {
            recvData.rfinish();                   // prevent warnings spam
            return;
        }

        // if we boarded a transport, add us to it
        if (plrMover)
        {
            if (!plrMover->GetTransport())
            {
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                {
                    plrMover->m_transport = transport;
                    transport->AddPassenger(plrMover);
                }
            }
            else if (plrMover->GetTransport()->GetGUID() != movementInfo.transport.guid)
            {
                bool foundNewTransport = false;
                plrMover->m_transport->RemovePassenger(plrMover);
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                {
                    foundNewTransport = true;
                    plrMover->m_transport = transport;
                    transport->AddPassenger(plrMover);
                }

                if (!foundNewTransport)
                {
                    plrMover->m_transport = NULL;
                    movementInfo.transport.Reset();
                }
            }
        }

        if (!mover->GetTransport() && !mover->GetVehicle())
            movementInfo.flags &= ~MOVEMENTFLAG_ONTRANSPORT;
    }
    else if (plrMover && plrMover->GetTransport()) // if we were on a transport, leave
    {
        plrMover->m_transport->RemovePassenger(plrMover);
        plrMover->m_transport = NULL;
        movementInfo.transport.Reset();
    }

    if (plrMover && ((movementInfo.flags & MOVEMENTFLAG_SWIMMING) != 0) != plrMover->IsInWater())
    {
        // now client not include swimming flag in case jumping under water
        plrMover->SetInWater(!plrMover->IsInWater() || plrMover->GetBaseMap()->IsUnderWater(movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), movementInfo.pos.GetPositionZ()));
    }
	// Dont allow to turn on walking if charming other player
	if (mover->GetGUID() != _player->GetGUID())
		movementInfo.flags &= ~MOVEMENTFLAG_WALKING;

    uint32 mstime = getMSTime();

    /*----------------------*/
    if(m_clientTimeDelay == 0)
        m_clientTimeDelay = mstime > movementInfo.time ? std::min(mstime - movementInfo.time, (uint32)100) : 0;

    // Xinef: do not allow to move with UNIT_FLAG_DISABLE_MOVE
    if (mover->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE))
	{
		// Xinef: skip moving packets
		if (movementInfo.HasMovementFlag(MOVEMENTFLAG_MASK_MOVING))
			return;
		movementInfo.pos.Relocate(mover->GetPositionX(), mover->GetPositionY(), mover->GetPositionZ());

		if (mover->GetTypeId() == TYPEID_UNIT)
		{
			movementInfo.transport.guid = mover->m_movementInfo.transport.guid;
			movementInfo.transport.pos.Relocate(mover->m_movementInfo.transport.pos.GetPositionX(), mover->m_movementInfo.transport.pos.GetPositionY(), mover->m_movementInfo.transport.pos.GetPositionZ());
			movementInfo.transport.seat = mover->m_movementInfo.transport.seat;
		}
    }
    
    // AntiCheat 
    if (plrMover) {
        if (!checkMovement(movementInfo, opcode)) 
        {
            plrMover->m_anti_AlarmCount += 5;
            if (plrMover->m_anti_AlarmCount > 20 && !plrMover->IsGameMaster())
                plrMover->GetSession()->KickPlayer();
        } else {
            plrMover->m_anti_AlarmCount -= plrMover->m_anti_AlarmCount > 0 ? 1 : 0;
        }
    }

    /* process position-change */
    WorldPacket data(opcode, recvData.size());

    //movementInfo.time = movementInfo.time + m_clientTimeDelay + MOVEMENT_PACKET_TIME_DELAY;
    movementInfo.time = mstime; // pussywizard: set to time of relocation (server time), constant addition may smoothen movement clientside, but client sees target on different position than the real serverside position

    movementInfo.guid = mover->GetGUID();
    WriteMovementInfo(&data, &movementInfo);
    mover->SendMessageToSet(&data, _player);

    mover->m_movementInfo = movementInfo;

    // this is almost never true (pussywizard: only one packet when entering vehicle), normally use mover->IsVehicle()
    if (mover->GetVehicle())
    {
        mover->SetOrientation(movementInfo.pos.GetOrientation());
		mover->UpdatePosition(movementInfo.pos);
        return;
    }

	// pussywizard: previously always mover->UpdatePosition(movementInfo.pos);
	if (movementInfo.flags & MOVEMENTFLAG_ONTRANSPORT && mover->GetTransport())
	{
		float x, y, z, o;
		movementInfo.transport.pos.GetPosition(x, y, z, o);
		mover->GetTransport()->CalculatePassengerPosition(x, y, z, &o);
		mover->UpdatePosition(x, y, z, o);
	}
	else
		mover->UpdatePosition(movementInfo.pos);

	// fall damage generation (ignore in flight case that can be triggered also at lags in moment teleportation to another map).
	// Xinef: moved it here, previously StopMoving function called when player died relocated him to last saved coordinates (which were in air)
    if (opcode == MSG_MOVE_FALL_LAND 
            && plrMover 
            && !plrMover->IsInFlight() 
            && (!plrMover->GetTransport() || plrMover->GetTransport()->GetGOInfo()->type == GAMEOBJECT_TYPE_MO_TRANSPORT)
    )
        plrMover->HandleFall(movementInfo);

	// Xinef: interrupt parachutes upon falling or landing in water
	if (opcode == MSG_MOVE_FALL_LAND || opcode == MSG_MOVE_START_SWIM)
		mover->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_LANDING); // Parachutes


    if (plrMover)                                            // nothing is charmed, or player charmed
    {
        if (plrMover->IsSitState() && (movementInfo.flags & (MOVEMENTFLAG_MASK_MOVING | MOVEMENTFLAG_MASK_TURNING)))
            plrMover->SetStandState(UNIT_STAND_STATE_STAND);

        plrMover->UpdateFallInformationIfNeed(movementInfo, opcode);

        if (movementInfo.pos.GetPositionZ() < -500.0f)
            if (!plrMover->GetBattleground() || !plrMover->GetBattleground()->HandlePlayerUnderMap(_player))
            {
                if (plrMover->IsAlive())
                {
                    plrMover->EnvironmentalDamage(DAMAGE_FALL_TO_VOID, GetPlayer()->GetMaxHealth());
                    // player can be alive if GM
                    if (plrMover->IsAlive())
                        plrMover->KillPlayer();
                }
                plrMover->StopMoving(true); // pussywizard: moving corpse can't release spirit
            }
    }
}

void WorldSession::HandleForceSpeedChangeAck(WorldPacket &recvData)
{
    /* extract packet */
    ObjectGuid guid;
    uint32 unk1;
    float  newspeed;

    recvData >> guid.ReadAsPacked();

    // now can skip not our packet
    if (_player->GetGUID() != guid)
    {
        recvData.rfinish();                   // prevent warnings spam
        return;
    }

    // continue parse packet

    recvData >> unk1;                                      // counter or moveEvent

    MovementInfo movementInfo;
    movementInfo.guid = guid;
    ReadMovementInfo(recvData, &movementInfo);

    recvData >> newspeed;
    /*----------------*/

    // client ACK send one packet for mounted/run case and need skip all except last from its
    // in other cases anti-cheat check can be fail in false case
    UnitMoveType move_type;
    UnitMoveType force_move_type;

    static char const* move_type_name[MAX_MOVE_TYPE] = {  "Walk", "Run", "RunBack", "Swim", "SwimBack", "TurnRate", "Flight", "FlightBack", "PitchRate" };

    switch (recvData.GetOpcode())
    {
        case CMSG_FORCE_WALK_SPEED_CHANGE_ACK:          move_type = MOVE_WALK;          force_move_type = MOVE_WALK;        break;
        case CMSG_FORCE_RUN_SPEED_CHANGE_ACK:           move_type = MOVE_RUN;           force_move_type = MOVE_RUN;         break;
        case CMSG_FORCE_RUN_BACK_SPEED_CHANGE_ACK:      move_type = MOVE_RUN_BACK;      force_move_type = MOVE_RUN_BACK;    break;
        case CMSG_FORCE_SWIM_SPEED_CHANGE_ACK:          move_type = MOVE_SWIM;          force_move_type = MOVE_SWIM;        break;
        case CMSG_FORCE_SWIM_BACK_SPEED_CHANGE_ACK:     move_type = MOVE_SWIM_BACK;     force_move_type = MOVE_SWIM_BACK;   break;
        case CMSG_FORCE_TURN_RATE_CHANGE_ACK:           move_type = MOVE_TURN_RATE;     force_move_type = MOVE_TURN_RATE;   break;
        case CMSG_FORCE_FLIGHT_SPEED_CHANGE_ACK:        move_type = MOVE_FLIGHT;        force_move_type = MOVE_FLIGHT;      break;
        case CMSG_FORCE_FLIGHT_BACK_SPEED_CHANGE_ACK:   move_type = MOVE_FLIGHT_BACK;   force_move_type = MOVE_FLIGHT_BACK; break;
        case CMSG_FORCE_PITCH_RATE_CHANGE_ACK:          move_type = MOVE_PITCH_RATE;    force_move_type = MOVE_PITCH_RATE;  break;
        default:
            TC_LOG_ERROR("network", "WorldSession::HandleForceSpeedChangeAck: Unknown move type opcode: %u", recvData.GetOpcode());
            return;
    }

    // skip all forced speed changes except last and unexpected
    // in run/mounted case used one ACK and it must be skipped.m_forced_speed_changes[MOVE_RUN} store both.
    if (_player->m_forced_speed_changes[force_move_type] > 0)
    {
        --_player->m_forced_speed_changes[force_move_type];
        if (_player->m_forced_speed_changes[force_move_type] > 0)
            return;
    }

    if (!_player->GetTransport() && std::fabs(_player->GetSpeed(move_type) - newspeed) > 0.01f)
    {
        if (_player->GetSpeed(move_type) > newspeed)         // must be greater - just correct
        {
            TC_LOG_ERROR("network", "%sSpeedChange player %s is NOT correct (must be %f instead %f), force set to correct value",
                move_type_name[move_type], _player->GetName().c_str(), _player->GetSpeed(move_type), newspeed);
            _player->SetSpeed(move_type, _player->GetSpeedRate(move_type), true);
        }
        else                                                // must be lesser - cheating
        {
            TC_LOG_DEBUG("misc", "Player %s from account id %u kicked for incorrect speed (must be %f instead %f)",
                _player->GetName().c_str(), _player->GetSession()->GetAccountId(), _player->GetSpeed(move_type), newspeed);
            _player->GetSession()->KickPlayer();
        }
    }
}

void WorldSession::HandleSetActiveMoverOpcode(WorldPacket &recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_SET_ACTIVE_MOVER");

    ObjectGuid guid;
    recvData >> guid;

    if (GetPlayer()->IsInWorld())
        if (_player->m_mover->GetGUID() != guid)
            TC_LOG_DEBUG("network", "HandleSetActiveMoverOpcode: incorrect mover guid: mover is %s and should be %s" , guid.ToString().c_str(), _player->m_mover->GetGUID().ToString().c_str());
}

void WorldSession::HandleMoveNotActiveMover(WorldPacket &recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_MOVE_NOT_ACTIVE_MOVER");

    ObjectGuid old_mover_guid;
    recvData >> old_mover_guid.ReadAsPacked();

    MovementInfo mi;
    ReadMovementInfo(recvData, &mi);

    mi.guid = old_mover_guid;

    _player->m_movementInfo = mi;
}

void WorldSession::HandleMountSpecialAnimOpcode(WorldPacket& /*recvData*/)
{
    WorldPacket data(SMSG_MOUNTSPECIAL_ANIM, 8);
    data << uint64(GetPlayer()->GetGUID());

    GetPlayer()->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveKnockBackAck(WorldPacket & recvData)
{
    TC_LOG_DEBUG("network", "CMSG_MOVE_KNOCK_BACK_ACK");

    ObjectGuid guid;
    recvData >> guid.ReadAsPacked();

    if (_player->m_mover->GetGUID() != guid)
        return;

    recvData.read_skip<uint32>();                          // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);
	// Save movement flags
    _player->SetUnitMovementFlags(movementInfo.flags);
    #ifdef ANTICHEAT_DEBUG
    TC_LOG_WARN("cheat", "%s CMSG_MOVE_KNOCK_BACK_ACK: time: %d, fall time: %d | xyzo: %f,%f,%fo(%f) flags[%X] Vspeed: %f, Hspeed: %f", GetPlayer()->GetName(), movementInfo.time, movementInfo.fallTime, movementInfo.pos.m_positionX, movementInfo.pos.m_positionY, movementInfo.pos.m_positionZ, movementInfo.pos.m_orientation, movementInfo.flags, movementInfo.jump.zspeed, movementInfo.jump.xyspeed);
    #endif

    _player->m_movementInfo = movementInfo;
	_player->m_anti_Last_HSpeed = movementInfo.jump.xyspeed;
    _player->m_anti_Last_VSpeed = movementInfo.jump.zspeed < 3.2f ? movementInfo.jump.zspeed - 1.0f : 3.2f;
 
    const uint32 dt = (_player->m_anti_Last_VSpeed < 0) ? int(ceil(_player->m_anti_Last_VSpeed/-25)*1000) : int(ceil(_player->m_anti_Last_VSpeed/25)*1000);
    _player->m_anti_LastSpeedChangeTime = movementInfo.time + dt + 1000;

    WorldPacket data(MSG_MOVE_KNOCK_BACK, 66);
    data << guid.WriteAsPacked();
    _player->BuildMovementPacket(&data);

    // knockback specific info
    data << movementInfo.jump.sinAngle;
    data << movementInfo.jump.cosAngle;
    data << movementInfo.jump.xyspeed;
    data << movementInfo.jump.zspeed;

    _player->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveHoverAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "CMSG_MOVE_HOVER_ACK");

    ObjectGuid guid;                                        // guid - unused
    recvData >> guid.ReadAsPacked();

    recvData.read_skip<uint32>();                           // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);

    recvData.read_skip<uint32>();                           // unk2
}

void WorldSession::HandleMoveWaterWalkAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "CMSG_MOVE_WATER_WALK_ACK");

    ObjectGuid guid;                                        // guid - unused
    recvData >> guid.ReadAsPacked();

    recvData.read_skip<uint32>();                           // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);

    recvData.read_skip<uint32>();                           // unk2
}

void WorldSession::HandleSummonResponseOpcode(WorldPacket& recvData)
{
    if (!_player->IsAlive() || _player->IsInCombat())
        return;

    ObjectGuid summoner_guid;
    bool agree;
    recvData >> summoner_guid;
    recvData >> agree;

    _player->SummonIfPossible(agree);
}

bool WorldSession::checkMovement(MovementInfo const& movementInfo, uint16 opcode) 
{
    Unit* mover = _player->m_mover;

    ASSERT(mover != NULL);                      // there must always be a mover
    
    Player* plrMover;

    if (mover->IsCharmed()) {
        plrMover = mover->GetCharmerOrOwnerPlayerOrPlayerItself();
        return true;
    } else {
        plrMover = mover->ToPlayer();
    }

    bool isDev = plrMover->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_DEVELOPER);

    int64 currentServerTime = getMSTime();
    int64 currentClientTime = movementInfo.time;

    int64 lastClientTime = plrMover->m_anti_LastClientTime;
    int64 lastServerTime = plrMover->m_anti_LastServerTime;

    plrMover->m_anti_LastServerTime = currentServerTime;
    plrMover->m_anti_LastClientTime = currentClientTime;

    // Есть последнее время
    if (plrMover->m_anti_LastClientTime > 0 && plrMover->m_anti_LastServerTime > 0)
    {
        int64 deltaClientTime = currentClientTime - lastClientTime;
        int64 deltaServerTime = currentServerTime - lastServerTime;

        // Дельта клиента не должна быть отрицательной.
        if (deltaClientTime < 0) 
        {
            return false;
        }

        int64 latency = plrMover->GetSession()->GetLatency() * 2 > 150 ? plrMover->GetSession()->GetLatency() * 2 : 150;

        // Дельты сервера и клиента разняться больше чем на задержку клиента
        // Так быть не должно
        if (deltaServerTime - deltaClientTime > latency || deltaServerTime - deltaClientTime < latency * (-1))
        {
            if (isDev) {
                ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка дельты непройдена: dST(%i) dCT(%i), dd(%i), latency*2(%i)", 
                                                                        deltaServerTime, 
                                                                        deltaClientTime,
                                                                        deltaServerTime - deltaClientTime,
                                                                        latency);
            }
            return false;
        }

        if (!plrMover->m_taxi.GetTaxiDestination()) // pass if not taxi
        {
            UnitMoveType moveType;

            if (movementInfo.flags & MOVEMENTFLAG_FLYING) {
                moveType = movementInfo.flags & MOVEMENTFLAG_BACKWARD ? MOVE_FLIGHT_BACK : MOVE_FLIGHT;
            } else if (movementInfo.flags & MOVEMENTFLAG_SWIMMING) {
                moveType = movementInfo.flags & MOVEMENTFLAG_BACKWARD ? plrMover->IsInWater() ? MOVE_SWIM_BACK : MOVE_RUN_BACK : MOVE_SWIM;
            } else if (movementInfo.flags & MOVEMENTFLAG_WALKING) {
                moveType = MOVE_WALK;
            }
            // hmm... in first time after login player has MOVE_SWIMBACK instead MOVE_WALKBACK
            else
                moveType = movementInfo.flags & MOVEMENTFLAG_BACKWARD ? MOVE_RUN_BACK : MOVE_RUN;

            float current_speed = mover->GetSpeed(moveType);
            
            // Так ли обязательна проверка на транспорт?
            float delta_x = plrMover->m_transport ? 0 : movementInfo.pos.GetPositionX() - plrMover->GetPositionX();
            float delta_y = plrMover->m_transport ? 0 : movementInfo.pos.GetPositionY() - plrMover->GetPositionY();
            float delta_z = plrMover->m_transport ? 0 : movementInfo.pos.GetPositionZ() - plrMover->GetPositionZ();

            if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] m_transport(%b) delta_x(%f), delta_y(%f), delta_z(%f)", !!plrMover->m_transport, delta_x, delta_y, delta_z);
                    
            // Проверим на полет.
            if (moveType == MOVE_FLIGHT_BACK || moveType == MOVE_FLIGHT) {
                    
                bool hasFlyAuras = plrMover->HasAuraType(SPELL_AURA_FLY) 
                                || plrMover->HasAuraType(SPELL_AURA_MOD_INCREASE_VEHICLE_FLIGHT_SPEED)
                                || plrMover->HasAuraType(SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED) 
                                || plrMover->HasAuraType(SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED)
                                || plrMover->HasAuraType(SPELL_AURA_MOD_MOUNTED_FLIGHT_SPEED_ALWAYS) 
                                || plrMover->HasAuraType(SPELL_AURA_MOD_FLIGHT_SPEED_NOT_STACK);
                
                // drop fly hack
                if (!hasFlyAuras && !plrMover->IsGameMaster()) { 
                    if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка аур полета не пройдена");
                    {
                        WorldPacket data(SMSG_MOVE_SET_CAN_FLY, 12);
                        data << plrMover->GetPackGUID();
                        data << uint32(0);
                        SendPacket(&data);
                    }
                    {
                        WorldPacket data(SMSG_MOVE_UNSET_CAN_FLY, 12);
                        data << plrMover->GetPackGUID();
                        data << uint32(0);
                        SendPacket(&data);
                    }
                    return false;
                }

                // Пройденная дистанция из пакета
                float calculatedDistance = (sqrt(pow(delta_x, 2) + pow(delta_y, 2) + pow(delta_z, 2)));

                // Максимально возможная дистанция 
                float allowedDistance = (deltaServerTime * current_speed) / 1000 + 4;

                allowedDistance = allowedDistance > 50 ? 50 : allowedDistance;

                if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка дистанции полета: calculated(%f), allowed(%f)", calculatedDistance, allowedDistance);

                return allowedDistance >= calculatedDistance;
            }

            if (moveType == MOVE_SWIM_BACK || moveType == MOVE_SWIM) 
            {
                const bool inWater = mover->IsInWater();
                const bool underWater = plrMover->IsUnderWater();
                const bool swim = inWater || underWater;

                // Сложно плавать не в воде, о_О
                if (!swim)
                    return false;

                // Пройденная дистанция из пакета
                float calculatedDistance = (sqrt(pow(delta_x, 2) + pow(delta_y, 2) + pow(delta_z, 2)));

                // Максимально возможная дистанция 
                float allowedDistance = (deltaServerTime * current_speed) / 1000 + 2;

                allowedDistance = allowedDistance > 50 ? 50 : allowedDistance;

                if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка дистанции плаванья: calculated(%f), allowed(%f)", calculatedDistance, allowedDistance);

                return allowedDistance >= calculatedDistance;
            }

            const bool waterWalkFlag = movementInfo.flags & MOVEMENTFLAG_WATERWALKING;
            const bool hasWaterWalkAuras = plrMover->HasAuraType(SPELL_AURA_WATER_WALK) || plrMover->HasAuraType(SPELL_AURA_GHOST);

            if (waterWalkFlag && !hasWaterWalkAuras) {
                if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка хождения по воде не пройдена");

                return false;
            }
            
            const float ground_z = plrMover->GetMap()->GetWaterOrGroundLevel(movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), movementInfo.pos.GetPositionZ(), NULL, true);

            // possibly falling
            if (delta_z < 0) 
            {
                float allowedDistance = 0;
                // Падаем
                if (abs(movementInfo.pos.GetPositionZ() - ground_z) > 1) {
                    // 30 максимальная скорость падения, однако расчет получется уж очень странным.
                    allowedDistance = (deltaServerTime * (current_speed - delta_z * 2 + 10)) / 1000 + 2;
                } else {
                    allowedDistance = (deltaServerTime * current_speed) / 1000 + 2;
                }

                allowedDistance = allowedDistance > 50 ? 50 : allowedDistance;

                // Пройденная дистанция из пакета
                float calculatedDistance = (sqrt(pow(delta_x, 2) + pow(delta_y, 2) + pow(delta_z, 2)));

                if (calculatedDistance > 50) {
                    WorldPacket data;
                    plrMover->SendTeleportAckPacket();
                    plrMover->BuildHeartBeatMsg(&data);
                    plrMover->SendMessageToSet(&data, true);
                    return false;
                }
                
                if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка дистанции передвижения вниз: calculated(%f), allowed(%f)", calculatedDistance, allowedDistance);

                return allowedDistance >= calculatedDistance;

            } else {
                if (abs(movementInfo.pos.GetPositionZ() - ground_z) > 2) {
                    if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Предположительно FlyHack");
                    
                    if (!plrMover->IsGameMaster()) {
                        return false;
                    }
                }

                const float delta2d = pow(delta_x, 2) + pow(delta_y, 2);
                const float tg_z = delta2d > 0 ? pow(delta_z, 2) / delta2d : -99999; //Вычисляем тангенс угла

                if (tg_z > 2.48) // Wall Climb Constant 
                {
                    if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка wallclimb не пройдена: tg_z(%f)", tg_z);
                    return false;
                }

                float calculatedDistance = (sqrt(pow(delta_x, 2) + pow(delta_y, 2) + pow(delta_z, 2)));

                // Максимально возможная дистанция 
                float allowedDistance = (deltaServerTime * current_speed) / 1000 + 2;

                allowedDistance = allowedDistance > 50 ? 50 : allowedDistance;
                
                if (calculatedDistance > 50) {
                    WorldPacket data;
                    plrMover->SendTeleportAckPacket();
                    plrMover->BuildHeartBeatMsg(&data);
                    plrMover->SendMessageToSet(&data, true);
                    return false;
                }

                if (isDev) ChatHandler(plrMover->GetSession()).PSendSysMessage("[ANTICHEAT] Проверка дистанции передвижения вверх: calculated(%f), allowed(%f)", calculatedDistance, allowedDistance);

                return allowedDistance >= calculatedDistance;
            }
        }
    }

    // All checks passed
    return true;
}
