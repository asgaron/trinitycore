﻿/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Item_Scripts
SD%Complete: 100
SDComment: Items for a range of different items. See content below (in script)
SDCategory: Items
EndScriptData */

/* ContentData
item_nether_wraith_beacon(i31742)   Summons creatures for quest Becoming a Spellfire Tailor (q10832)
item_flying_machine(i34060, i34061)  Engineering crafted flying machines
item_gor_dreks_ointment(i30175)     Protecting Our Own(q10488)
item_only_for_flight                Items which should only useable while flying
EndContentData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Spell.h"
#include "Player.h"
#include "GossipDef.h"
#include "Item.h"
#include "ScriptedGossip.h"
#include "WorldSession.h"

/*#####
# item_only_for_flight
#####*/

enum OnlyForFlight
{
    SPELL_ARCANE_CHARGES    = 45072
};

class item_only_for_flight : public ItemScript
{
public:
    item_only_for_flight() : ItemScript("item_only_for_flight") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
    {
        uint32 itemId = item->GetEntry();
        bool disabled = false;

        //for special scripts
        switch (itemId)
        {
           case 24538:
                if (player->GetAreaId() != 3628)
                    disabled = true;
                    break;
           case 34489:
                if (player->GetZoneId() != 4080)
                    disabled = true;
                    break;
           case 34475:
                if (const SpellInfo* spellInfo = sSpellMgr->GetSpellInfo(SPELL_ARCANE_CHARGES))
                    Spell::SendCastResult(player, spellInfo, 1, SPELL_FAILED_NOT_ON_GROUND);
                    break;
        }

        // allow use in flight only
        if (player->IsInFlight() && !disabled)
            return false;

        // error
        player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
        return true;
    }
};

/*#####
# item_nether_wraith_beacon
#####*/

class item_nether_wraith_beacon : public ItemScript
{
public:
    item_nether_wraith_beacon() : ItemScript("item_nether_wraith_beacon") { }

    bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const& /*targets*/) override
    {
        if (player->GetQuestStatus(10832) == QUEST_STATUS_INCOMPLETE)
        {
            if (Creature* nether = player->SummonCreature(22408, player->GetPositionX(), player->GetPositionY()+20, player->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 180000))
                nether->AI()->AttackStart(player);

            if (Creature* nether = player->SummonCreature(22408, player->GetPositionX(), player->GetPositionY()-20, player->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 180000))
                nether->AI()->AttackStart(player);
        }
        return false;
    }
};

/*#####
# item_gor_dreks_ointment
#####*/

class item_gor_dreks_ointment : public ItemScript
{
public:
    item_gor_dreks_ointment() : ItemScript("item_gor_dreks_ointment") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& targets) override
    {
        if (targets.GetUnitTarget() && targets.GetUnitTarget()->GetTypeId() == TYPEID_UNIT &&
            targets.GetUnitTarget()->GetEntry() == 20748 && !targets.GetUnitTarget()->HasAura(32578))
            return false;

        player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
        return true;
    }
};

/*#####
# item_incendiary_explosives
#####*/

class item_incendiary_explosives : public ItemScript
{
public:
    item_incendiary_explosives() : ItemScript("item_incendiary_explosives") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const & /*targets*/) override
    {
        if (player->FindNearestCreature(26248, 15) || player->FindNearestCreature(26249, 15))
            return false;
        else
        {
            player->SendEquipError(EQUIP_ERR_OUT_OF_RANGE, item, NULL);
            return true;
        }
    }
};

/*#####
# item_mysterious_egg
#####*/

class item_mysterious_egg : public ItemScript
{
public:
    item_mysterious_egg() : ItemScript("item_mysterious_egg") { }

    bool OnExpire(Player* player, ItemTemplate const* /*pItemProto*/) override
    {
        ItemPosCountVec dest;
        uint8 msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, 39883, 1); // Cracked Egg
        if (msg == EQUIP_ERR_OK)
            player->StoreNewItem(dest, 39883, true, Item::GenerateItemRandomPropertyId(39883));

        return true;
    }
};

/*#####
# item_disgusting_jar
#####*/

class item_disgusting_jar : public ItemScript
{
public:
    item_disgusting_jar() : ItemScript("item_disgusting_jar") { }

    bool OnExpire(Player* player, ItemTemplate const* /*pItemProto*/) override
    {
        ItemPosCountVec dest;
        uint8 msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, 44718, 1); // Ripe Disgusting Jar
        if (msg == EQUIP_ERR_OK)
            player->StoreNewItem(dest, 44718, true, Item::GenerateItemRandomPropertyId(44718));

        return true;
    }
};

/*#####
# item_pile_fake_furs
#####*/

enum PileFakeFur
{
    GO_CARIBOU_TRAP_1                                      = 187982,
    GO_CARIBOU_TRAP_2                                      = 187995,
    GO_CARIBOU_TRAP_3                                      = 187996,
    GO_CARIBOU_TRAP_4                                      = 187997,
    GO_CARIBOU_TRAP_5                                      = 187998,
    GO_CARIBOU_TRAP_6                                      = 187999,
    GO_CARIBOU_TRAP_7                                      = 188000,
    GO_CARIBOU_TRAP_8                                      = 188001,
    GO_CARIBOU_TRAP_9                                      = 188002,
    GO_CARIBOU_TRAP_10                                     = 188003,
    GO_CARIBOU_TRAP_11                                     = 188004,
    GO_CARIBOU_TRAP_12                                     = 188005,
    GO_CARIBOU_TRAP_13                                     = 188006,
    GO_CARIBOU_TRAP_14                                     = 188007,
    GO_CARIBOU_TRAP_15                                     = 188008,
    GO_HIGH_QUALITY_FUR                                    = 187983,
    NPC_NESINGWARY_TRAPPER                                 = 25835
};

#define CaribouTrapsNum 15
const uint32 CaribouTraps[CaribouTrapsNum] =
{
    GO_CARIBOU_TRAP_1, GO_CARIBOU_TRAP_2, GO_CARIBOU_TRAP_3, GO_CARIBOU_TRAP_4, GO_CARIBOU_TRAP_5,
    GO_CARIBOU_TRAP_6, GO_CARIBOU_TRAP_7, GO_CARIBOU_TRAP_8, GO_CARIBOU_TRAP_9, GO_CARIBOU_TRAP_10,
    GO_CARIBOU_TRAP_11, GO_CARIBOU_TRAP_12, GO_CARIBOU_TRAP_13, GO_CARIBOU_TRAP_14, GO_CARIBOU_TRAP_15,
};

class item_pile_fake_furs : public ItemScript
{
public:
    item_pile_fake_furs() : ItemScript("item_pile_fake_furs") { }

    bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const & /*targets*/) override
    {
        GameObject* go = NULL;
        for (uint8 i = 0; i < CaribouTrapsNum; ++i)
        {
            go = player->FindNearestGameObject(CaribouTraps[i], 5.0f);
            if (go)
                break;
        }

        if (!go)
            return false;

        if (go->FindNearestCreature(NPC_NESINGWARY_TRAPPER, 10.0f, true) || go->FindNearestCreature(NPC_NESINGWARY_TRAPPER, 10.0f, false) || go->FindNearestGameObject(GO_HIGH_QUALITY_FUR, 2.0f))
            return true;

        float x, y, z;
        go->GetClosePoint(x, y, z, go->GetObjectSize() / 3, 7.0f);
        go->SummonGameObject(GO_HIGH_QUALITY_FUR, go->GetPositionX(), go->GetPositionY(), go->GetPositionZ(), 0, 0, 0, 0, 0, 1);
        if (TempSummon* summon = player->SummonCreature(NPC_NESINGWARY_TRAPPER, x, y, z, go->GetOrientation(), TEMPSUMMON_DEAD_DESPAWN, 1000))
        {
            summon->SetVisible(false);
            summon->SetReactState(REACT_PASSIVE);
            summon->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
        }
        return false;
    }
};

/*#####
# item_petrov_cluster_bombs
#####*/

enum PetrovClusterBombs
{
    SPELL_PETROV_BOMB           = 42406,
    AREA_ID_SHATTERED_STRAITS   = 4064,
    ZONE_ID_HOWLING             = 495
};

class item_petrov_cluster_bombs : public ItemScript
{
public:
    item_petrov_cluster_bombs() : ItemScript("item_petrov_cluster_bombs") { }

    bool OnUse(Player* player, Item* item, const SpellCastTargets & /*targets*/) override
    {
        if (player->GetZoneId() != ZONE_ID_HOWLING)
            return false;

        if (!player->GetTransport() || player->GetAreaId() != AREA_ID_SHATTERED_STRAITS)
        {
            player->SendEquipError(EQUIP_ERR_NONE, item, NULL);

            if (const SpellInfo* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PETROV_BOMB))
                Spell::SendCastResult(player, spellInfo, 1, SPELL_FAILED_NOT_HERE);

            return true;
        }

        return false;
    }
};

/*######
# item_dehta_trap_smasher
# For quest 11876, Help Those That Cannot Help Themselves
######*/
enum HelpThemselves
{
    QUEST_CANNOT_HELP_THEMSELVES                  =  11876,
    NPC_TRAPPED_MAMMOTH_CALF                      =  25850,
    GO_MAMMOTH_TRAP_1                             = 188022,
    GO_MAMMOTH_TRAP_2                             = 188024,
    GO_MAMMOTH_TRAP_3                             = 188025,
    GO_MAMMOTH_TRAP_4                             = 188026,
    GO_MAMMOTH_TRAP_5                             = 188027,
    GO_MAMMOTH_TRAP_6                             = 188028,
    GO_MAMMOTH_TRAP_7                             = 188029,
    GO_MAMMOTH_TRAP_8                             = 188030,
    GO_MAMMOTH_TRAP_9                             = 188031,
    GO_MAMMOTH_TRAP_10                            = 188032,
    GO_MAMMOTH_TRAP_11                            = 188033,
    GO_MAMMOTH_TRAP_12                            = 188034,
    GO_MAMMOTH_TRAP_13                            = 188035,
    GO_MAMMOTH_TRAP_14                            = 188036,
    GO_MAMMOTH_TRAP_15                            = 188037,
    GO_MAMMOTH_TRAP_16                            = 188038,
    GO_MAMMOTH_TRAP_17                            = 188039,
    GO_MAMMOTH_TRAP_18                            = 188040,
    GO_MAMMOTH_TRAP_19                            = 188041,
    GO_MAMMOTH_TRAP_20                            = 188042,
    GO_MAMMOTH_TRAP_21                            = 188043,
    GO_MAMMOTH_TRAP_22                            = 188044,
};

#define MammothTrapsNum 22
const uint32 MammothTraps[MammothTrapsNum] =
{
    GO_MAMMOTH_TRAP_1, GO_MAMMOTH_TRAP_2, GO_MAMMOTH_TRAP_3, GO_MAMMOTH_TRAP_4, GO_MAMMOTH_TRAP_5,
    GO_MAMMOTH_TRAP_6, GO_MAMMOTH_TRAP_7, GO_MAMMOTH_TRAP_8, GO_MAMMOTH_TRAP_9, GO_MAMMOTH_TRAP_10,
    GO_MAMMOTH_TRAP_11, GO_MAMMOTH_TRAP_12, GO_MAMMOTH_TRAP_13, GO_MAMMOTH_TRAP_14, GO_MAMMOTH_TRAP_15,
    GO_MAMMOTH_TRAP_16, GO_MAMMOTH_TRAP_17, GO_MAMMOTH_TRAP_18, GO_MAMMOTH_TRAP_19, GO_MAMMOTH_TRAP_20,
    GO_MAMMOTH_TRAP_21, GO_MAMMOTH_TRAP_22
};

class item_dehta_trap_smasher : public ItemScript
{
public:
    item_dehta_trap_smasher() : ItemScript("item_dehta_trap_smasher") { }

    bool OnUse(Player* player, Item* /*item*/, const SpellCastTargets & /*targets*/) override
    {
        if (player->GetQuestStatus(QUEST_CANNOT_HELP_THEMSELVES) != QUEST_STATUS_INCOMPLETE)
            return false;

        Creature* pMammoth = player->FindNearestCreature(NPC_TRAPPED_MAMMOTH_CALF, 5.0f);
        if (!pMammoth)
            return false;

        GameObject* pTrap = NULL;
        for (uint8 i = 0; i < MammothTrapsNum; ++i)
        {
            pTrap = player->FindNearestGameObject(MammothTraps[i], 11.0f);
            if (pTrap)
            {
                pMammoth->AI()->DoAction(1);
                pTrap->SetGoState(GO_STATE_READY);
                player->KilledMonsterCredit(NPC_TRAPPED_MAMMOTH_CALF);
                return true;
            }
        }
        return false;
    }
};

enum TheEmissary
{
    QUEST_THE_EMISSARY      =   11626,
    NPC_LEVIROTH            =   26452
};

class item_trident_of_nazjan : public ItemScript
{
public:
    item_trident_of_nazjan() : ItemScript("item_Trident_of_Nazjan") { }

    bool OnUse(Player* player, Item* item, const SpellCastTargets & /*targets*/) override
    {
        if (player->GetQuestStatus(QUEST_THE_EMISSARY) == QUEST_STATUS_INCOMPLETE)
        {
            if (Creature* pLeviroth = player->FindNearestCreature(NPC_LEVIROTH, 10.0f)) // spell range
            {
                pLeviroth->AI()->AttackStart(player);
                return false;
            } else
                player->SendEquipError(EQUIP_ERR_OUT_OF_RANGE, item, NULL);
        } else
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
        return true;
    }
};

enum CapturedFrog
{
    QUEST_THE_PERFECT_SPIES      = 25444,
    NPC_VANIRAS_SENTRY_TOTEM     = 40187
};

class item_captured_frog : public ItemScript
{
public:
    item_captured_frog() : ItemScript("item_captured_frog") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
    {
        if (player->GetQuestStatus(QUEST_THE_PERFECT_SPIES) == QUEST_STATUS_INCOMPLETE)
        {
            if (player->FindNearestCreature(NPC_VANIRAS_SENTRY_TOTEM, 10.0f))
                return false;
            else
                player->SendEquipError(EQUIP_ERR_OUT_OF_RANGE, item, NULL);
        }
        else
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
        return true;
    }
};

enum porter_menu
{
	MENU_SENDER = 1000,
	MENU_DUNGEON = 2000,
	MENU_WORLD_BOSSES = 3000,
	MENU_AUTOEVENTS = 4000,
	MENU_PROFFS = 5000,
	MENU_QUESTS = 6000,
	MENU_BACK = 7000,


	ACTION_CAPITAL = MENU_SENDER + 1,
	ACTION_DUNGEON,
	ACTION_WORLD_BOSSES,
	ACTION_AUTOEVENTS,
	ACTION_PROFFS,
	ACTION_QUESTS,
	ACTION_SUM_LOC,
	ACTION_SILVERMOON,

	ACTION_DUNGEON_T2 = MENU_DUNGEON + 1,
	ACTION_DUNGEON_T3,
	ACTION_DUNGEON_T4,
	ACTION_DUNGEON_T5,

	ACTION_WORLD_BOSSES_1 = MENU_WORLD_BOSSES + 1,
	ACTION_WORLD_BOSSES_2,
	ACTION_WORLD_BOSSES_3,
	ACTION_WORLD_BOSSES_4,

};

class item_porter : public ItemScript
{
public:
	item_porter() : ItemScript("item_porter") { }

	bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
	{
		if (player->IsInCombat())
			return false;

		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/INV_Misc_Coin_05:30|t Столица", MENU_SENDER, ACTION_CAPITAL, "Телепортироваться в Столицу?", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_killxenemies_generalsroom:26|t Подземелья", MENU_SENDER, ACTION_DUNGEON, "", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_trueavshutout:26|t Мировые боссы", MENU_SENDER, ACTION_WORLD_BOSSES, "", 0, 0);
//		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_dungeon_outland_dungeon_hero:26|t Авто-Ивенты", MENU_SENDER, ACTION_AUTOEVENTS, "", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Spell_arcane_portalsilvermoon:30|t Луносвет", MENU_SENDER, ACTION_SILVERMOON, "Телепортироваться в Луносвет?", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_overcome500disadvantage:26|t Профессии", MENU_SENDER, ACTION_PROFFS, "", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_quests_completed_08:26|t Задания", MENU_SENDER, ACTION_QUESTS, "", 0, 0);
		player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_zone_zuldrak_03:26|t Сумеречная гряда", MENU_SENDER, ACTION_SUM_LOC, "Телепортироваться в Сумеречную Гряду?", 0, 0);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
		return false;
	}

	void OnGossipSelect(Player* player, Item* item, uint32 sender, uint32 action) override
	{
//		player->GetSession()->SendNotification("Проверка, sender - %u, action - %u", sender, action);
		player->PlayerTalkClass->ClearMenus();

		switch (sender)
		{
		case MENU_SENDER:
			switch (action)
			{
			case ACTION_SILVERMOON:
				player->TeleportTo(530, 9969.302734f, -7080.395996f, 47.708767f, 0.915967f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_CAPITAL:
				player->TeleportTo(530, -3809.959f, -11467.799f, -138.339f, 4.20872f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_DUNGEON:
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/rex1:30|t Логово Рексара [т2]", MENU_DUNGEON, ACTION_DUNGEON_T2, "Телепортироваться в Логово Рексара?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_kelthuzad_01:30|t Обитель Льда [т3]", MENU_DUNGEON, ACTION_DUNGEON_T3, "Телепортироваться в Обитель Льда?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_chiefukorzsandscalp:30|t Крепость Драк'Тарон [т4]", MENU_DUNGEON, ACTION_DUNGEON_T4, "Телепортироваться в Крепость Драк'Тарон?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_generalvezax_01:30|t Непроглядная Пучина [т5]", MENU_DUNGEON, ACTION_DUNGEON_T5, "Телепортироваться в Непроглядную Пучину?", 0, 0);
				player->ADD_GOSSIP_ITEM(2, "|TInterface/ICONS/Inv_misc_map_01:30|t Назад", MENU_BACK, 0);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
				break;
			case ACTION_WORLD_BOSSES:
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_murmur:30|t Вестник Бури", MENU_WORLD_BOSSES, ACTION_WORLD_BOSSES_1, "Телепортироваться к Вестнику Бури?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_thaddius:30|t Таддиус Адский Вопль", MENU_DUNGEON, ACTION_WORLD_BOSSES_2, "Телепортироваться к Таддиусу?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_cthun:30|t Пожиратель Cкверны", MENU_DUNGEON, ACTION_WORLD_BOSSES_3, "Телепортироваться к Пожирателю Скверны?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Spell_fire_incinerate:30|t Энге, повелитель Хаоса", MENU_DUNGEON, ACTION_WORLD_BOSSES_4, "Телепортироваться к Энге?", 0, 0);
				player->ADD_GOSSIP_ITEM(2, "|TInterface/ICONS/Inv_misc_map_01:30|t Назад", MENU_BACK, 0);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
				break;
			case ACTION_AUTOEVENTS:
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_boss_illidan:30|t В поисках Иллидана |cff00ff00[Легко]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 1, "Телепортироваться на Ивент \"В Поисках Иллидана\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_dungeon_icecrown_hallsofreflection:30|t Путь Праведника |cffffff00[Средне]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 2, "Телепортироваться на Ивент \"Путь Праведника\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Ability_rogue_cheatdeath:30|t Джамп |cffffff00[Средне]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 3, "Телепортироваться на Ивент \"Джамп\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_dungeon_ulduarraid_archway_01:30|t Лестница Черной Горы |cff00ff00[Легко]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 4, "Телепортироваться на Ивент \"Лестница Черной Горы\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_dungeon_icecrown_frostwinghalls:30|t Лабиринт Ульдуара |cffff0000[Сложно]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 5, "Телепортироваться на Ивент \"Лабиринт Ульдуара\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Ability_warrior_swordandboard:30|t Сопровождение каравана |cffffff00[Средне]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 6, "Телепортироваться на Ивент \"Сопровождение каравана\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_dungeon_gundrak_heroic:30|t Побег из Зул'Гуруба |cff00ff00[Легко]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 7, "Телепортироваться на Ивент \"Побег\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Ability_hunter_focusedaim:30|t Удачная посадка |cff00ff00[Легко]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 8, "Телепортироваться на Ивент \"Удачная посадка\"?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_bg_grab_cap_flagunderxseconds:30|t Смертельная гонка |cffffff00[Средне]|r", MENU_AUTOEVENTS, MENU_AUTOEVENTS + 9, "Телепортироваться на Ивент \"Смертельная гонка\"?", 0, 0);
				player->ADD_GOSSIP_ITEM(2, "|TInterface/ICONS/Inv_misc_map_01:30|t Назад", MENU_BACK, 0);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
				break;
			case ACTION_PROFFS:
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/redmettal:30|t Шахты Экзодара (горное дело)", MENU_PROFFS, MENU_PROFFS + 1, "Телепортироваться к шахте Экзодара?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/darckmetal:30|t Шахты Манагорн Ара (горное дело)", MENU_PROFFS, MENU_PROFFS + 2, "Телепортироваться к шахте Манагорн Ара?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_dungeon_outland_dungeonmaster:30|t Врата смерти (снятие шкур)", MENU_PROFFS, MENU_PROFFS + 3, "Телепортироваться к Вратам смерти?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Inv_misc_head_dragon_green:30|t Святилище драконов (травничество)", MENU_PROFFS, MENU_PROFFS + 4, "Телепортироваться к Святилищу драконов?", 0, 0);
				player->ADD_GOSSIP_ITEM(2, "|TInterface/ICONS/Inv_misc_map_01:30|t Назад", MENU_BACK, 0);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
				break;
			case ACTION_QUESTS:
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Achievement_profession_fishing_outlandangler:30|t Рыбалка", MENU_QUESTS, MENU_QUESTS + 1, "Телепортироваться на Рыбалку?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Inv_ammo_bullet_04:30|t Подготовка", MENU_QUESTS, MENU_QUESTS + 2, "Телепортироваться на Подготовку?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Inv_cask_03:30|t Провиант", MENU_QUESTS, MENU_QUESTS + 3, "Телепортироваться на Провиант?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Inv_mushroom_02:30|t Знахарь вечнозеленой рощи", MENU_QUESTS, MENU_QUESTS + 4, "Телепортироваться к Знахарю вечнозеленой рощи?", 0, 0);
				player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Inv_misc_head_dragon_01:30|t Драконий остров", MENU_QUESTS, MENU_QUESTS + 5, "Телепортироваться на Драконий остров?", 0, 0);
				player->ADD_GOSSIP_ITEM(2, "|TInterface/ICONS/Inv_misc_map_01:30|t Назад", MENU_BACK, 0);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
				break;
			case ACTION_SUM_LOC:
				player->TeleportTo(530, -1287.290f, 9593.889f, 205.612f, 2.5974f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_DUNGEON:
			switch (action)
			{
			case ACTION_DUNGEON_T2:
				player->TeleportTo(530, 3533.4799f, 5100.2099f, 3.65321f, 5.60149f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_DUNGEON_T3:
				player->TeleportTo(571, 5607.109f, 2022.81f, 798.084f, 3.84923f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_DUNGEON_T4:
				player->TeleportTo(571, 4774.259f, -2033.01f, 229.3789f, 1.57822f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_DUNGEON_T5:
				player->TeleportTo(1, 4247.140f, 738.2579f, -25.965939f, 1.29983f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_WORLD_BOSSES:
			switch (action)
			{
			case ACTION_WORLD_BOSSES_1:
				player->TeleportTo(571, 8129.959951f, -1907.41f, 1736.41f, 5.56118f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_WORLD_BOSSES_2:
				player->TeleportTo(571, 3672.540039f, 2101.79f, 20.278999f, 4.28031f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_WORLD_BOSSES_3:
				player->TeleportTo(530, 367.604f, 3364.5f, 63.935398f, 4.98589f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case ACTION_WORLD_BOSSES_4:
				player->TeleportTo(0, -11211.2f, -1463.65f, 3.38956f, 1.628185f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_AUTOEVENTS:
			switch (action)
			{
			case MENU_AUTOEVENTS + 1:
				player->TeleportTo(564, 702.7180f, 222.197f, 125.007f, 1.54876f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 2:
				player->TeleportTo(668, 5236.0097f, 1932.64f, 707.695f, 0.8f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 3:
				player->TeleportTo(530, -3753.55f, -11695.599f, -105.93f, 0.04733f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 4:
				player->TeleportTo(0, -8100.41f, -1639.1999f, 132.7359f, 0.876f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 5:
				player->TeleportTo(603, -886.344971f, -148.9889f, 458.859f, 6.28077f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 6:
				player->TeleportTo(47, 1943.949f, 1550.2299f, 82.186f, 0.5047f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 7:
				player->TeleportTo(309, -11789.4f, -1623.75f, 54.722f, 4.76972f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 8:
				player->TeleportTo(1, 497.674f, -326.948f, 377.603f, 5.04325f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_AUTOEVENTS + 9:
				player->TeleportTo(548, -85.086f, -85.604f, 9.0755f, 5.23813f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_PROFFS:
			switch (action)
			{
			case MENU_PROFFS + 1:
				player->TeleportTo(530, -3676.78f, -11580.2f, -124.687f, 1.0f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_PROFFS + 2:
				player->TeleportTo(530, 3806.620f, 4013.979f, 123.680f, 6.21f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_PROFFS + 3:
				player->TeleportTo(530, 2241.899f, 5518.93f, 162.619f, 4.6455f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_PROFFS + 4:
				player->TeleportTo(571, 2719.83f, -9.762990f, 1.251f, 6.0835f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_QUESTS:
			switch (action)
			{
			case MENU_QUESTS + 1:
				player->TeleportTo(1, -2185.07f, -737.4619f, -13.5531f, 0.6069f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_QUESTS + 2:
				player->TeleportTo(530, -4195.83f, -12584.9f, 35.491814f, 4.58202f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_QUESTS + 3:
				player->TeleportTo(530, 2241.899f, 5518.93f, 162.619f, 4.6455f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_QUESTS + 4:
				player->TeleportTo(530, -4301.160156f, -11414.7f, 1.219f, 2.128f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_QUESTS + 5:
				player->TeleportTo(530, -221.0f, 5477.68f, 21.95f, 1.2651f);
				player->CLOSE_GOSSIP_MENU();
				return;
			case MENU_QUESTS + 6:
				player->TeleportTo(530, -1385.4f, -12349.7998f, 11.3912f, 5.64095f);
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			break;
		case MENU_BACK:
			player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/INV_Misc_Coin_05:30|t Столица", MENU_SENDER, ACTION_CAPITAL, "Телепортироваться в Столицу?", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_killxenemies_generalsroom:26|t Подземелья", MENU_SENDER, ACTION_DUNGEON, "", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_trueavshutout:26|t Мировые боссы", MENU_SENDER, ACTION_WORLD_BOSSES, "", 0, 0);
//			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_dungeon_outland_dungeon_hero:26|t Авто-Ивенты", MENU_SENDER, ACTION_AUTOEVENTS, "", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(4, "|TInterface/ICONS/Spell_arcane_portalsilvermoon:30|t Луносвет", MENU_SENDER, ACTION_SILVERMOON, "Телепортироваться в Луносвет?", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_bg_overcome500disadvantage:26|t Профессии", MENU_SENDER, ACTION_PROFFS, "", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_quests_completed_08:26|t Задания", MENU_SENDER, ACTION_QUESTS, "", 0, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|TInterface/ICONS/Achievement_zone_zuldrak_03:26|t Сумеречная гряда", MENU_SENDER, ACTION_SUM_LOC, "Телепортироваться в Сумеречную Гряду?", 0, 0);
			break;
		}
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
	}
};

void AddSC_item_scripts()
{
    new item_only_for_flight();
    new item_nether_wraith_beacon();
    new item_gor_dreks_ointment();
    new item_incendiary_explosives();
    new item_mysterious_egg();
    new item_disgusting_jar();
    new item_pile_fake_furs();
    new item_petrov_cluster_bombs();
    new item_dehta_trap_smasher();
    new item_trident_of_nazjan();
    new item_captured_frog();
	new item_porter();
}
