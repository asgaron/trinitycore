﻿#include "GroupMgr.h"
#include "ScriptMgr.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GameEventMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Unit.h"
#include "GameObject.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "InstanceScript.h"
#include "CombatAI.h"
#include "PassiveAI.h"
#include "Chat.h"
#include "DBCStructure.h"
#include "DBCStores.h"
#include "ObjectMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

enum Paladin_event_structure {
	DEAD_PALADIN_ENTRY = 211810,
	SPIRIT_ENTRY = 211811,

	TEXT_SPIRIT_FREE = 100010, //Свободу темный душам!
	TEXT_ENTER_COMBAT		//Я подарю вам освобождение!
};

enum boss_dead_paladin_entries
{
	ENTRY_DEAD_PALADIN = 211810,
	ENTRY_SPIRIT = 211811,
};

enum boss_dead_paladin_events
{
	EVENT_SPELL_EXECUTE = 1, //AoE Stun
	EVENT_SPELL_EXECUTER, //AoE Damage

	EVENT_SPELL_DARK_STUN, //Stun for a single target
	EVENT_SPELL_DARK_JUSTICE, //DarkJustice - must be purged
	EVENT_SPELL_HEAL, //69634
	EVENT_SPELL_DARK_BUFF, //DarkBuff - must be stealed / purged
	EVENT_SPELL_DARK_BUBBLE, //Bubble. Yeah. Amma paladin
};

enum boss_dead_paladin_spells
{
	SPELL_AURA_LESS_DAMAGE = 700000,

	SPELL_EXECUTE = 701000,
	SPELL_EXECUTE_DAMAGE,
	SPELL_DARK_JUSTICE,

	SPELL_DARK_STUN = 58154,
	SPELL_HEAL = 69634,
	SPELL_DARK_BUFF = 69391,
	SPELL_DARK_BUBBLE = 64505,

	SPELL_ABSORB_ALL_NOT_SPIRIT = 701005,
	SPELL_NEAREST_DEATH
};


class boss_dead_paladin : public CreatureScript
{
public:
	boss_dead_paladin() : CreatureScript("boss_dead_paladin") { }

	struct boss_dead_paladinAI : public BossAI //npc 32353
	{
		boss_dead_paladinAI(Creature* creature) : BossAI(creature, 1)
		{
			Initialize();
		}

		void Initialize() {
			SpiritCreated = false;
			died = false;
			lights_triggered = false;
			me->AddAura(SPELL_AURA_LESS_DAMAGE, me);
		}

		void Reset() override
		{
			if (died)
			{
				me->Yell("Силы возвращаются ко мне. А вы, глупцы, стали моей следующей каплей в моем море могущества!", LANG_UNIVERSAL);
				me->Kill(spirit);
			}
			_Reset();
			Initialize();
			me->SetPosition(me->GetHomePosition());
		}

		void EnterEvadeMode() override {
			Reset();
		}

		void EnterCombat(Unit*) override
		{
			me->Yell(TEXT_ENTER_COMBAT);

			events.ScheduleEvent(EVENT_SPELL_DARK_STUN, urand(5000, 7000));
			events.ScheduleEvent(EVENT_SPELL_DARK_JUSTICE, urand(12000, 15000));
			events.ScheduleEvent(EVENT_SPELL_HEAL, urand(24000, 29000));
			events.ScheduleEvent(EVENT_SPELL_EXECUTE, 32000);
			events.ScheduleEvent(EVENT_SPELL_DARK_BUBBLE, urand(5, 30) * IN_MILLISECONDS);

		}

		void ExecuteEvent(uint32 eventId) override
		{
			switch (eventId)
			{
			//All Phases
			case EVENT_SPELL_EXECUTE:
				me->Yell("Умрите, глупцы!", LANG_UNIVERSAL);
				DoCast(SPELL_EXECUTE);
				DoCast(SPELL_EXECUTE_DAMAGE);
				events.ScheduleEvent(EVENT_SPELL_EXECUTE, 50000);
				break;
			case EVENT_SPELL_DARK_STUN:
				DoCast(SPELL_DARK_STUN);
				events.ScheduleEvent(EVENT_SPELL_DARK_STUN, urand(5000, 7000));
				break;
			case EVENT_SPELL_DARK_JUSTICE:
				DoCast(SPELL_DARK_JUSTICE);
				events.ScheduleEvent(EVENT_SPELL_DARK_JUSTICE, urand(12000, 15000));
				break;
			case EVENT_SPELL_HEAL:
				DoCast(SPELL_HEAL);
				events.ScheduleEvent(EVENT_SPELL_HEAL, urand(24000, 29000));
				break;
			case EVENT_SPELL_DARK_BUBBLE:
				DoCast(SPELL_DARK_BUBBLE);
				events.ScheduleEvent(EVENT_SPELL_DARK_BUBBLE, urand(15, 30) * IN_MILLISECONDS);
				break;
			
			//Phase Lights
			case EVENT_SPELL_DARK_BUFF:
				DoCast(SPELL_DARK_BUFF);
				events.ScheduleEvent(EVENT_SPELL_DARK_BUFF, 20000);
				break;
			}
		}

		void DamageTaken(Unit* done_by, uint32 &damage) override
		{
			if (HealthBelowPct(51) && !lights_triggered)
			{
				lights_triggered = true;
				spirit = me->SummonCreature(SPIRIT_ENTRY, spirit_pos);
				DoCast(SPELL_ABSORB_ALL_NOT_SPIRIT);
				me->Yell("Это... Невозможно... Свет... Покидает меня...", LANG_UNIVERSAL);
				events.ScheduleEvent(EVENT_SPELL_DARK_BUFF, 5000);
			}

			if (damage >= me->GetHealth() && !died)
			{
				died = true;
				damage = 0;
				me->Yell("МЕРЕДИЛ, МАСТЕР, МНЕ ТРЕБУЕТСЯ ТВОЕ БЛАГОСЛОВЛЕНИЕ!!! ДАЙ МНЕ СИЛЫ, ЧТОБЫ ОДАЛЕТЬ ВРАГОВ ТВОИХ!!!", LANG_UNIVERSAL);
				DoCast(SPELL_NEAREST_DEATH);
				//Больше не кастим никакие спелы, просто три удара. Победа или смерть!
				events.CancelEventGroup(0);
			}
		}

	private:
		Creature* spirit;
		Position spirit_pos = Position(274.509064f, -100.029732f, 28.869146f, 3.145755f);
		bool lights_triggered = false, SpiritCreated = false, died = false;
	};

	
	CreatureAI* GetAI(Creature* creature) const override
	{
		return new boss_dead_paladinAI(creature);
	}
};

class spirit_cre : public CreatureScript
{
public:
	spirit_cre() : CreatureScript("npc_dead_paladin_spirit") { }

	struct spirit_creAI : public ScriptedAI
	{
		spirit_creAI(Creature* creature) : ScriptedAI(creature) { }

		void JustSummoned(Creature* summoner) override
		{
			paladin = summoner;
			me->SetMaxHealth(paladin->GetMaxHealth());
			me->SetHealth(me->GetMaxHealth() / 3);
			me->setFaction(35);
			DoCast(paladin, 72735);
		}

		void HealReceived(Unit* healer, uint32& heal) override
		{
			if (paladin && paladin->IsAlive()) me->DealDamage(paladin, heal);

			if (heal >= me->GetMaxHealth() - me->GetHealth()) heal = 0;
		}

		void DamageTaken(Unit* done_by, uint32 &damage) override
		{
			damage = 0;
		}

	private:
		Creature* paladin;
	};

	CreatureAI* GetAI(Creature* creature) const override
	{
		return new spirit_creAI(creature);
	}
};

//SPELL_ABSORB_ALL_NOT_SPIRIT = 701005,
class spell_absorb_not_spirit : public SpellScriptLoader
{
public:
	spell_absorb_not_spirit() : SpellScriptLoader("spell_absorb_not_spirit") { }

	class spell_absorb_not_spirit_AuraScript : public AuraScript
	{
		PrepareAuraScript(spell_absorb_not_spirit_AuraScript);

		bool Validate(SpellInfo const* /*spell*/) override
		{
			if (!sSpellMgr->GetSpellInfo(SPELL_ABSORB_ALL_NOT_SPIRIT))
				return false;
			return true;
		}

		void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
		{
			amount = -1;
		}

		void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
		{
			if (!(dmgInfo.GetAttacker()->ToCreature() && dmgInfo.GetAttacker()->ToCreature()->GetEntry() == ENTRY_SPIRIT))
				absorbAmount = dmgInfo.GetDamage();
		}

		void Register() override
		{
			DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_absorb_not_spirit_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
			OnEffectAbsorb += AuraEffectAbsorbFn(spell_absorb_not_spirit_AuraScript::Absorb, EFFECT_0);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_absorb_not_spirit_AuraScript();
	}
};

//SPELL_NEAREST_DEATH = 701006
class spell_nearest_death : public SpellScriptLoader
{
public:
	spell_nearest_death() : SpellScriptLoader("spell_nearest_death") { }

	class spell_nearest_death_AuraScript : public AuraScript
	{
		PrepareAuraScript(spell_nearest_death_AuraScript);

		bool Validate(SpellInfo const* /*spellInfo*/) override
		{
			if (!sSpellMgr->GetSpellInfo(SPELL_NEAREST_DEATH))
				return false;
			return true;
		}

		void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
		{
			GetCaster()->RemoveAura(SPELL_ABSORB_ALL_NOT_SPIRIT);
		}

		void Register() override
		{
			AfterEffectRemove += AuraEffectRemoveFn(spell_nearest_death_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_nearest_death_AuraScript();
	}
};

void AddSC_boss_dead_palladin()
{
	new boss_dead_paladin();
	new spirit_cre();
	new spell_absorb_not_spirit();
	new spell_nearest_death();
}