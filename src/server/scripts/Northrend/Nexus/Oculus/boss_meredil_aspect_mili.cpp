/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Meredil Aspect Milli
SD%Complete: 100
SDComment: 
SDCategory: Instance Script
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "oculus.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellInfo.h"

enum Spells
{
	SPELL_AURA_LESS_DAMAGE = 700000,
	SPELL_AURA_LESS_DAMAGE_2 = 710002,

	SPELL_CLEIMOR = 720000,
	SPELL_RAZREZ,
	SPELL_DOT,

	SPELL_MANTRA_VIS,
	SPELL_MANTRA_HEAL,
	SPELL_MANTRA_PARRY,
	SPELL_MANTRA_SPEED,

	SPELL_VIHR,
	SPELL_VIHR_PROC,

	SPELL_SUMMON_VIS,
	SPELL_AURA_NOMAGICDAMAGE,
	SPELL_MYST_FORM,

	SPELL_CLINOK,

	SPELL_TREPKA,
	SPELL_TREPKA_VZRIV,

	SPELL_MANA_DRAIN,
	SPELL_MANA_DRAIN_PROC
};

enum Phases 
{
	PHASE_ONE = 1,
	PHASE_TWO,
	PHASE_THREE,
};

enum EventsAndAction 
{
	EVENT_CAST_CLEIMOR = 1,
	EVENT_CAST_RAZREZ,
	EVENT_CAST_DOT,
	EVENT_CAST_MANTRA,
	EVENT_CAST_VIHR,
	EVENT_CAST_CLINOK,
	EVENT_CAST_TREPKA,
	EVENT_PHASE_THREE_START,

	ACTION_HELL = 1,
};

class boss_meredil_aspect_mili : public CreatureScript
{
public:
	boss_meredil_aspect_mili() : CreatureScript("boss_meredil_aspect_mili") { }

	struct boss_meredil_aspect_miliAI : public BossAI
	{
		boss_meredil_aspect_miliAI(Creature* creature) : BossAI(creature, DATA_ASPECT_MILI)
		{
			Initialize();
		}

		void Initialize()
		{
			DoCast(SPELL_AURA_LESS_DAMAGE);
			DoCast(SPELL_AURA_LESS_DAMAGE_2);

			events.SetPhase(0);

			events.ScheduleEvent(EVENT_CAST_CLEIMOR, 8 * IN_MILLISECONDS, 0, 0);
			events.ScheduleEvent(EVENT_CAST_RAZREZ, 4 * IN_MILLISECONDS, 0, 0);
			events.ScheduleEvent(EVENT_CAST_DOT, 10 * IN_MILLISECONDS, 0, 0);
			events.ScheduleEvent(EVENT_CAST_MANTRA, 30 * IN_MILLISECONDS, 0, 0);

			events.ScheduleEvent(EVENT_CAST_VIHR, urand(40, 45) * IN_MILLISECONDS, 0, PHASE_TWO);
			events.ScheduleEvent(EVENT_CAST_CLINOK, 2 * 60 * IN_MILLISECONDS, 0, PHASE_TWO);

			events.ScheduleEvent(EVENT_CAST_TREPKA, urand(3, 4), 0, PHASE_THREE);
		}

		void Reset() override
		{
			_Reset();
			Initialize();
		}

		void EnterCombat(Unit* /*who*/) override
		{
			_EnterCombat();

			events.SetPhase(PHASE_ONE);

			if (!me->HasAura(SPELL_AURA_LESS_DAMAGE))
				me->AddAura(SPELL_AURA_LESS_DAMAGE, me);
			if (!me->HasAura(SPELL_AURA_LESS_DAMAGE_2))
				me->AddAura(SPELL_AURA_LESS_DAMAGE_2, me);
		}

		void DamageTaken(Unit* /*attacker*/, uint32& damage) override
		{
			if (me->GetHealthPct() < 75 && events.IsInPhase(PHASE_ONE))
				events.SetPhase(PHASE_TWO);
			else if (me->GetHealthPct() <= 50 && events.IsInPhase(PHASE_TWO))
			{
				events.SetPhase(PHASE_THREE);
				me->CastStop();
				DoCast(SPELL_SUMMON_VIS); //Обязательно прервать все касты посередине. Чтобы не дай бог эта хуйня не сбилась
				events.ScheduleEvent(EVENT_PHASE_THREE_START, 30 * IN_MILLISECONDS);
			} 
			else if (me->GetHealthPct() <= 40) {
				me->RemoveAura(SPELL_AURA_NOMAGICDAMAGE);
				me->AddAura(SPELL_MANA_DRAIN, me);
			}
		}

		void ExecuteEvent(uint32 eventId) override
		{
			switch (eventId) {
				case EVENT_CAST_CLEIMOR: 
					DoCast(SPELL_CLEIMOR);
					events.ScheduleEvent(EVENT_CAST_CLEIMOR, 8 * IN_MILLISECONDS, 0, 0);
					break;
				case EVENT_CAST_CLINOK: 
					DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_CLINOK);
					events.ScheduleEvent(EVENT_CAST_CLINOK, 2 * 60 * IN_MILLISECONDS, 0, PHASE_TWO);
					break;
				case EVENT_CAST_DOT:
					DoCast(SPELL_DOT);
					events.ScheduleEvent(EVENT_CAST_DOT, 10 * IN_MILLISECONDS, 0, 0);
					break;
				case EVENT_CAST_MANTRA:
					DoCast(SPELL_MANTRA_HEAL + urand(0, 2));
					events.ScheduleEvent(EVENT_CAST_MANTRA, 30 * IN_MILLISECONDS, 0, 0);
					break;
				case EVENT_CAST_RAZREZ:
					DoCast(SPELL_RAZREZ);
					events.ScheduleEvent(EVENT_CAST_RAZREZ, 4 * IN_MILLISECONDS, 0, 0);
					break;
				case EVENT_CAST_TREPKA:
					DoCast(SPELL_TREPKA);
					events.ScheduleEvent(EVENT_CAST_TREPKA, urand(3, 4), 0, PHASE_THREE);
					break;
				case EVENT_CAST_VIHR:
					DoCast(SPELL_VIHR);
					events.ScheduleEvent(EVENT_CAST_VIHR, urand(40, 45) * IN_MILLISECONDS, 0, PHASE_TWO);
					break;
			}
		}
	};

	CreatureAI* GetAI(Creature* creature) const override
	{
		return GetOculusAI<boss_meredil_aspect_miliAI>(creature);
	}
};

class spell_clinok : public SpellScriptLoader
{
public:
	spell_clinok() : SpellScriptLoader("spell_clinok") { }

	class spell_clinok_SpellScript : public SpellScript
	{
		PrepareSpellScript(spell_clinok_SpellScript);

		bool Validate(SpellInfo const* /*spellInfo*/) override
		{
			if (!sSpellMgr->GetSpellInfo(SPELL_CLINOK))
				return false;
			return true;
		}

		void HandleDamage(SpellEffIndex /*effIndex*/)
		{
			Unit* target = GetHitUnit();
			SetHitDamage(target->GetMaxHealth());

			Unit* caster = GetCaster();
			caster->SummonGameObject(300100, target->GetPositionX(), target->GetPositionY(), target->GetPositionZ() - 2, 0, 0, 0, 0, 0, 5);
		}

		void Register() override
		{
			OnEffectHitTarget += SpellEffectFn(spell_clinok_SpellScript::HandleDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
		}
	};

	SpellScript* GetSpellScript() const override
	{
		return new spell_clinok_SpellScript();
	}
};

class spell_trepka : public SpellScriptLoader
{
public:
	spell_trepka() : SpellScriptLoader("spell_trepka") { }

	class spell_trepka_AuraScript : public AuraScript
	{
		PrepareAuraScript(spell_trepka_AuraScript);

		bool Validate(SpellInfo const* /*spellInfo*/) override
		{
			if (!sSpellMgr->GetSpellInfo(SPELL_TREPKA))
				return false;
			return true;
		}

		void HandleDispel(DispelInfo* /*dispelInfo*/)
		{
			if (Unit* caster = GetCaster())
				if (Unit* target = GetUnitOwner())
					caster->CastSpell(target, SPELL_TREPKA_VZRIV);
		}

		void Register() override
		{
			AfterDispel += AuraDispelFn(spell_trepka_AuraScript::HandleDispel);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_trepka_AuraScript();
	}
};

void AddSC_boss_meredil_aspect_mili()
{
	new boss_meredil_aspect_mili();
	new spell_clinok();
	new spell_trepka();
}