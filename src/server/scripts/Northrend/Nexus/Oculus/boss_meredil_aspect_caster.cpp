/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 /* ScriptData
 SDName: Meredil Aspect Milli
 SD%Complete: 100
 SDComment:
 SDCategory: Instance Script
 EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "oculus.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellInfo.h"

enum Spells
{
	SPELL_AURA_LESS_DAMAGE = 700000,
	SPELL_AURA_LESS_DAMAGE_2 = 710002,
};

enum Phases
{
	PHASE_ONE = 1,
	PHASE_TWO,
	PHASE_THREE,
};

enum EventsAndAction
{
};

class boss_meredil_aspect_caster : public CreatureScript
{
public:
	boss_meredil_aspect_caster() : CreatureScript("boss_meredil_aspect_caster") { }

	struct boss_meredil_aspect_casterAI : public BossAI
	{
		boss_meredil_aspect_casterAI(Creature* creature) : BossAI(creature, DATA_ASPECT_CASTER)
		{
			Initialize();
		}

		void Initialize()
		{
			DoCast(SPELL_AURA_LESS_DAMAGE);
			DoCast(SPELL_AURA_LESS_DAMAGE_2);

			events.SetPhase(0);
		}

		void Reset() override
		{
			_Reset();
			Initialize();
		}

		void EnterCombat(Unit* /*who*/) override
		{
			_EnterCombat();

			events.SetPhase(PHASE_ONE);

			if (!me->HasAura(SPELL_AURA_LESS_DAMAGE))
				me->AddAura(SPELL_AURA_LESS_DAMAGE, me);
			if (!me->HasAura(SPELL_AURA_LESS_DAMAGE_2))
				me->AddAura(SPELL_AURA_LESS_DAMAGE_2, me);
		}

		void DamageTaken(Unit* /*attacker*/, uint32& damage) override
		{

		}

		void ExecuteEvent(uint32 eventId) override
		{

		}
	};

	CreatureAI* GetAI(Creature* creature) const override
	{
		return GetOculusAI<boss_meredil_aspect_casterAI>(creature);
	}
};

void AddSC_boss_meredil_aspect_caster()
{
	new boss_meredil_aspect_caster();
}