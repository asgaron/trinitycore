/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "InstanceScript.h"
#include "WorldPacket.h"
#include "oculus.h"

DoorData const doorData[] =
{
    { 0,                    0,              DOOR_TYPE_ROOM,     BOUNDARY_NONE }
};

class instance_oculus : public InstanceMapScript
{
    public:
        instance_oculus() : InstanceMapScript(OculusScriptName, 578) { }

        struct instance_oculus_InstanceMapScript : public InstanceScript
        {
            instance_oculus_InstanceMapScript(Map* map) : InstanceScript(map)
            {
                SetHeaders(DataHeader);
                SetBossNumber(EncounterCount);
                LoadDoorData(doorData);
            }
			
			void OnPlayerEnter(Player* player) override
			{
				player_count = instance->GetPlayersCountExceptGMs();
				if (player_count >= 6 && healthcreature)
				{
					healthcreature->AddAura(HEALTH_AURA, healthcreature);
				}
			}

			void OnPlayerLeave(Player* player) override
			{
				player_count = instance->GetPlayersCountExceptGMs();
				if (player_count > 1 && healthcreature)
				{
					if (healthcreature->HasAura(HEALTH_AURA))
						healthcreature->RemoveAuraFromStack(HEALTH_AURA);
				}
			}

            void OnCreatureCreate(Creature* creature) override
            {
                switch (creature->GetEntry())
                {
					case NPC_ASPECT_MILI:
						AspectMiliGUID = creature->GetGUID();
						break;
					case NPC_ASPECT_CASTER:
						AspectCasterGUID = creature->GetGUID();
						break;
					case NPC_ASPECT_MANTR:
						AspectMantrGUID = creature->GetGUID();
						break;
					case NPC_MEREDIL:
						MeredilGUID = creature->GetGUID();
						break;
					case HEALTH_DUMMY_ID:
						healthcreature = creature;
						break;
					case NPC_MANASNAKE:
						manasnakes.push_back(creature);
						break;
                    default:
                        break;
                }
            }

            void OnGameObjectCreate(GameObject* go) override
            {
                switch (go->GetEntry())
                {
                    case GO_DRAGON_CAGE_DOOR:
                        AddDoor(go, true);
                        break;
                    default:
                        break;
                }
            }

            void OnGameObjectRemove(GameObject* go) override
            {
                switch (go->GetEntry())
                {
                    case GO_DRAGON_CAGE_DOOR:
                        AddDoor(go, false);
                        break;
                    default:
                        break;
                }
            }

            void OnUnitDeath(Unit* unit) override
            {
                Creature* creature = unit->ToCreature();
                if (!creature)
                    return;

				if (creature->GetEntry() == NPC_MANASNAKE && healthcreature) {
					healthcreature->RemoveAuraFromStack(SPELL_AURA_INCR_SPEED);
				}
            }

            void FillInitialWorldStates(WorldPacket& data) override
            {

            }

            void ProcessEvent(WorldObject* /*unit*/, uint32 eventId) override
            {

            }

            bool SetBossState(uint32 type, EncounterState state) override
            {
                if (!InstanceScript::SetBossState(type, state))
                    return false;

				if (type == DATA_ASPECT_MILI && state == DONE) {
					healthcreature->AddAura(SPELL_AURA_INCR_SPEED, healthcreature);
					healthcreature->AddAura(SPELL_AURA_INCR_SPEED, healthcreature);
					for (auto manasnake : manasnakes) {
						manasnake->AI()->DoAction(ACTION_START_TRASH);
					}
				}
                
                return true;
            }

            uint32 GetData(uint32 type) const override
            {
                return KILL_NO_CONSTRUCT;
            }

            ObjectGuid GetGuidData(uint32 type) const override
            {
                switch (type)
                {
                    case DATA_DRAKOS:
                        return DrakosGUID;
                    default:
                        break;
                }

                return ObjectGuid::Empty;
            }

            void Update(uint32 diff) override
            {
                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        default:
                            break;
                    }
                }
            }

        protected:
			ObjectGuid DrakosGUID, AspectMiliGUID, AspectCasterGUID, AspectMantrGUID, MeredilGUID;
			uint8 player_count;
			Creature* healthcreature;
			std::list<Creature*> manasnakes;

            EventMap events;
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new instance_oculus_InstanceMapScript(map);
        }
};

void AddSC_instance_oculus()
{
    new instance_oculus();
}
